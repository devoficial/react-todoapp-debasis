import React , { Component }from 'react';
import Sortable from "sortablejs";
import "./TodoList.css";
import List from "./List";

class  TodoList extends Component{
    componentDidMount(){
        this.$node = this.refs.sortable
        Sortable.create(this.$node)
    }
    
    render(){
        const {list, trash, name} = this.props;
        return (
        <div className="box">
        <p>{(name) ? name.toUpperCase() : "Add a item......"}</p>
        <ul ref="sortable" className="list-group">
            {list.map(({_id,name,isChecked} = item) => (
                <List key={_id} keyValue={{name,isChecked,_id}} trash={{trash,list}}/> 
            ))}
        </ul>
        </div>
        )
    }
}

export default TodoList;