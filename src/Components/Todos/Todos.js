import React, { Component } from "react";
import TodoList from "./TodoList";
import "./Todo.css";
import axios from "axios";

class Todos extends Component {

    constructor(props){
        super(props)
        this.state = {
            name:"",
            list:[]
        }
    }
    componentWillMount(){
        const url = `http://localhost:3000/api/todos`;
        axios(url)
        .then(data => {
            this.setState({list:data.data})
        })
    }
    handleChange(e){
        this.setState({name:e.target.value})
    }
    
    handleSubmit(e) {
        e.preventDefault()
        if(this.state.name){
            let name  = this.state.name
            axios({
                method:"post",
                url:`http://localhost:3000/api/todos?name=${name}`
            }).then((data) => {
                let nextState = [...this.state.list,data.data]
                this.setState({list:nextState, name:""})
            })
            
        }
    }
    handleTrash(e,i) {
        this.setState(prevState => {
            prevState.list.splice(i,1)[0]
            return {list:prevState.list}
        })
    }
    render(){
        return (
            <div className="container-fluid ">
                <form onSubmit={this.handleSubmit.bind(this)} className="form-build">
                    <input className="text" type="text" name="name" value={this.state.name} onChange={this.handleChange.bind(this)}/>
                    <input className="btn" type="submit" value="Submit" />
                </form>  
                <TodoList name={this.state.name}  list={this.state.list} trash={this.handleTrash.bind(this)}/>
               </div>
        )
    }
}

export default Todos;