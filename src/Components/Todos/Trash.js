import React from "react";
import { FaTrashAlt } from "react-icons/fa";
import "./Trash.css";
import axios from "axios";

const  Trash = (props) => (
            <span className="trash" onClick={(e) => handleClick(e,props)}>
                <FaTrashAlt/>   
            </span>
    )

function handleClick(e,props){
    e.stopPropagation()
    axios({
        method:"delete",
        url:`http://localhost:3000/api/todos?id=${props.trash.keyValue._id}`,
    }).then(data => {
        let item = props.trash.keyValue.name
        let index = props.trash.trash.list.findIndex((e,i) => e.name === item)
        let func = props.trash.trash.trash
        console.log(data)
        return func(e,index)
    })
}
export default Trash;