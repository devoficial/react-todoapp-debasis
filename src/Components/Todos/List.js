import React , { Component } from "react";
import { MdCheckBoxOutlineBlank, MdCheckBox} from "react-icons/md";
import Trash from "./Trash";
import axios from "axios";

class List extends Component{
    constructor(props){
        super(props)
        this.state = {
            isChecked:this.props.keyValue.isChecked,
        }
    }

    handleCheck(e) {
        axios({
            method:"put",
            url:`http://localhost:3000/api/todos/${this.props.keyValue._id}`,
        }).then(result => {
            this.setState((prevState) => {
                const newState = !prevState.isChecked;
                return {isChecked:newState}
            })
        })

    }
    makeCaptalize(str){
        return str.split(" ").map(word => `${word.charAt(0).toUpperCase()}${[...word].slice(1).join("").toLowerCase()}`).join(" ")
    }
    render() {
        const style = {
            fontStyle:(this.state.isChecked)?"italic":"normal",
            textDecoration:(this.state.isChecked)?
            "line-through":"none",
            opacity:(this.state.isChecked)?0.7:1
        }
        const { trash, list , keyValue } =  this.props
        return (
            <li className="list-group-item"
                onClick={this.handleCheck.bind(this)}
                style={style}
                >{this.makeCaptalize(this.props.keyValue.name)}
                <span className="icon">{(this.state.isChecked)?
                    <span>
                     <MdCheckBox/><Trash trash={{trash, list, keyValue}}/>
                    </span>
                : <MdCheckBoxOutlineBlank/>  }
                </span>
                </li> 
            
            )
    }
}
    
export default List;